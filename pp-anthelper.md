# Política de privacidad

El presente documento, establece los términos en que AntHelper usa la información que es proporcionada por los usuarios al momento de usar la aplicación.
Cuando usted llena los campos requeridos, la aplicación asegura que sólo se empleará de acuerdo a los términos de este documento.
Sin embargo, esta Política de Pricacidad puede ser actualizada con el tiempo, por lo que le recomiendo revisar continuamente esta página para asegurarse que está de acuerdo con dichos cambios.

## Información solicitada

**AntHelper** consume información del dispositivo del usuario, tales como fotografías, información de la colonia en cuestión.

## Uso de la información requerida

La aplicación emplean almacenamiento en el dispositivo que se muestra a su vez en las dirferentes pantallas de la aplicación, por lo que ningún dato proporcionado por usted es subido a Internet.

## Enlaces extermos

La aplicación contiene enlaces a otros sitios que así mismo están relacionados con el tema de la aplicación, tales como sus redes sociales.
También contiene enlaces publicitarios, mostrados en un pequeño banner, esto para monetizar la aplicación.
Una vez que usted da clic en los enlaces, esta saliendo de la aplicación, por lo tanto, no es responsabilidad de AntHelper los términos o condiciones de dichos sitios de terceros.

## Contacto

Carlos Galindo: *carlos.oficial.uaz@gmail.com*


*última modificación: 14 agosto 2018*
